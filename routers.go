package main

import (
	"go-fiber-crud/middleware"
	"go-fiber-crud/module/user/userbusiness"

	"github.com/gofiber/fiber/v2"
)

func setupRouters(app *fiber.App) {
	v1 := app.Group("/api/v1", middleware.CorsFilder())
	v1.Get("user", userbusiness.FindAllUser)
	v1.Get("user/:id", userbusiness.FindUser)
	v1.Post("user", userbusiness.NewCreateUser)
	v1.Delete("user/:id", userbusiness.DeleteUser)
	v1.Patch("user/:id", userbusiness.UpdateUser)
}
