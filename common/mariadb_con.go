package common

import (
	"fmt"
	"log"
	"os"
	"strconv"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

const connectionType = "mysql"

func initConnectionString(userdb string, password string, host string, port int, databaseName string) string {
	conn := fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8mb4&parseTime=True&loc=Local", userdb, password, host, port, databaseName)
	return conn
}

func loadConfig() string {
	userdb := os.Getenv("DB_USER")
	password := os.Getenv("DB_PASSWORD")
	host := os.Getenv("DB_HOST")
	databaseName := os.Getenv("DB_NAME")
	port, err := strconv.Atoi(os.Getenv("DB_PORT"))
	if err != nil {
		log.Fatalln(err)
	}
	return initConnectionString(userdb, password, host, port, databaseName)
}

func createConnection(url string) (db *gorm.DB) {
	db, error := gorm.Open(mysql.Open(url), &gorm.Config{})
	if error != nil {
		log.Fatalln(error)
	}
	return db
}

func GetConnection() (db *gorm.DB) {
	url := loadConfig()
	return createConnection(url)
}
