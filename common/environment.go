package common

import (
	"log"

	"github.com/joho/godotenv"
)

// Environment load environment file and attribute
func Environment() (err error) {
	errEnv := godotenv.Load(".env")
	if errEnv != nil {
		log.Fatalf("Error loading .env file")
	}
	return
}
