module go-fiber-crud

go 1.15

require (
	github.com/andybalholm/brotli v1.0.1 // indirect
	github.com/go-sql-driver/mysql v1.5.0 // indirect
	github.com/gofiber/fiber v1.14.6 // indirect
	github.com/gofiber/fiber/v2 v2.5.0
	github.com/joho/godotenv v1.3.0
	github.com/klauspost/compress v1.11.7 // indirect
	github.com/valyala/fasthttp v1.20.0 // indirect
	golang.org/x/sys v0.0.0-20210124154548-22da62e12c0c // indirect
	gorm.io/driver/mysql v1.0.4
	gorm.io/gorm v1.20.12
)
