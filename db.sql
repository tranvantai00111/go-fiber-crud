create database demo;

use demo;

create table users(
	id int AUTO_INCREMENT,
	user_name varchar(25),
	age int(2),
	address varchar(255),
	created_at timestamp,
	updated_at timestamp,	
	constraint user_pk primary key(id)
)