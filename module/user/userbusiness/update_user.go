package userbusiness

import (
	"go-fiber-crud/common"
	"go-fiber-crud/module/user/usermodel"
	"net/http"

	"github.com/gofiber/fiber/v2"
)

func UpdateUser(c *fiber.Ctx) error {
	id := c.Params("id")
	user := new(usermodel.User)
	if err := c.BodyParser(user); err != nil {
		return c.Status(http.StatusServiceUnavailable).JSON(user)
	}
	db := common.GetConnection()
	if err := db.Table(usermodel.User{}.TableName()).Where("id=?", id).Updates(&user).Error; err != nil {
		return c.Status(http.StatusBadRequest).JSON("Can't update id " + id)
	}

	return c.Status(http.StatusOK).JSON("Updated")
}
