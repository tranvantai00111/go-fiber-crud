package userbusiness

import (
	"go-fiber-crud/common"
	"go-fiber-crud/module/user/usermodel"
	"net/http"

	"github.com/gofiber/fiber/v2"
	"gorm.io/gorm"
)

func FindUser(c *fiber.Ctx) error {
	id := c.Params("id")
	db := common.GetConnection()

	var user usermodel.User

	if err := db.First(&user, id).Error; err != nil {
		return c.Status(http.StatusNotFound).JSON(gorm.ErrRecordNotFound.Error())
	}

	return c.Status(http.StatusOK).JSON(user)
}
