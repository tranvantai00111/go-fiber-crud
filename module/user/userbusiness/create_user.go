package userbusiness

import (
	"go-fiber-crud/common"
	"go-fiber-crud/module/user/usermodel"
	"net/http"

	"github.com/gofiber/fiber/v2"
)

func NewCreateUser(c *fiber.Ctx) error {
	db := common.GetConnection()
	user := new(usermodel.User)
	if err := c.BodyParser(user); err != nil {
		return c.Status(http.StatusServiceUnavailable).JSON(user)
	}
	db.Table(usermodel.User{}.TableName()).Create(&user)
	return c.Status(http.StatusOK).JSON(user)
}
