package userbusiness

import (
	"go-fiber-crud/common"
	"go-fiber-crud/module/user/usermodel"
	"net/http"

	"github.com/gofiber/fiber/v2"
)

func FindAllUser(c *fiber.Ctx) error {
	db := common.GetConnection()

	var users []usermodel.User

	if err := db.Find(&users).Error; err != nil {
		return c.Status(http.StatusBadRequest).JSON("Can't finds user")
	}
	if len(users) == 0 {
		return c.Status(http.StatusOK).JSON("User empty")
	}

	return c.Status(http.StatusOK).JSON(users)
}
