package userbusiness

import (
	"go-fiber-crud/common"
	"go-fiber-crud/module/user/usermodel"
	"net/http"

	"github.com/gofiber/fiber/v2"
)

func DeleteUser(c *fiber.Ctx) error {
	id := c.Params("id")
	db := common.GetConnection()

	var user usermodel.User

	db.First(&user, id)
	if user.Name == nil {
		return c.Status(http.StatusNotFound).JSON("Not found user with id" + id)
	}
	db.Table(usermodel.User{}.TableName()).Delete(&user)

	return c.Status(http.StatusOK).JSON(user)
}
