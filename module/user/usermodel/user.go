package usermodel

import "go-fiber-crud/common"

const EntityName = "User"

type User struct {
	common.SQLModel `json:",inline"`
	Name            *string `json:"name" gorm:"column:user_name"`
	Age             *int    `json:"age" gorm:"column:age"`
	Address         *string `json:"address" gorm:"column:address"`
}

func (User) TableName() string {
	return "users"
}
