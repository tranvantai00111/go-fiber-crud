#!/bin/bash

# install go fiber 
go get -u github.com/gofiber/fiber/v2

# install go mysql driver
go get github.com/go-sql-driver/mysql

# install go gorm , framework for execute mysql query
go get -u gorm.io/gorm

# install gorm mysql driver
go get -u gorm.io/driver/mysql

# install godotenv for load environment value
go get -u github.com/joho/godotenv