package main

import (
	"go-fiber-crud/common"

	"github.com/gofiber/fiber/v2"
)

func helloWorld(c *fiber.Ctx) error {
	return c.SendString("Hello, world!")
}
func main() {
	app := fiber.New()
	common.Environment()
	setupRouters(app)
	app.Listen(":3000")
}
